import crypto from 'crypto';
import fs from 'fs';
import path from 'path';

//openssl req -nodes -x509 -sha256 -newkey rsa:2048 -keyout server.key -out server.cert -days 365 -subj "/C=US/ST=VA/L=Woodbridge/O=None/OU=None/CN=localhost" -addext "subjectAltName = DNS:localhost,DNS:payload.localhost"

const { publicKey, privateKey } = crypto.generateKeyPairSync('rsa', {
    modulusLength: 2048,
    publicKeyEncoding: {
        type: 'spki',
        format: 'pem'
    },
    privateKeyEncoding: {
        type: 'pkcs8',
        format: 'pem'
    }
});

const csr = crypto.createSign('RSA-SHA256');
const csrInfo = 
`
-----BEGIN CERTIFICATE REQUEST-----
[req]
default_bits = 2048
prompt = no
default_md = sha256
req_extensions = req_ext
distinguished_name = dn

[ dn ]
CN=localhost
O=Local Host Development
OU=Local Host Development
L=Woodbridge
ST=Virginia
C=US

[ req_ext ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = localhost
DNS.2 = payload.localhost
`

csr.update(csrInfo);
csr.end();
const csrData = csr.sign(privateKey, 'base64');

fs.writeFileSync(path.join(__dirname, "..", "..", "certs", "CA.public"), publicKey);
fs.writeFileSync(path.join(__dirname, "..", "..", "certs", "CA.private"), privateKey);