import { execSync } from 'child_process';
import { readFileSync } from 'fs';

let npmPackage = JSON.parse(readFileSync("package.json", "utf-8"));
let cmd = `docker build -f Containerfile -t requirements-ui:${npmPackage.version} .`;
let result = execSync(cmd);
console.log(result.toString());