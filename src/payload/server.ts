import express from 'express'
import payload from 'payload'
import { randomUUID } from 'crypto';
import 'dotenv/config'

const PORT = process.env.PAYLOAD_PORT || 3001;
const PAYLOAD_SECRET = process.env.PAYLOAD_SECRET || randomUUID();

(async () => {
    const app = express()
    await payload.init({
        secret: PAYLOAD_SECRET,
        express: app
    })

    app.get("/", (req, res) => {
        res.redirect("/admin")
    })

    app.listen(PORT, async () => {
        console.log(`Express is now listening for incoming connections on port ${PORT}.`);
        console.log(`Use URL http://payload.localhost:${PORT}/admin to log in`)
    })
})();