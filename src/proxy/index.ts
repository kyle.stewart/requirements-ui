import express, {Request, Response, NextFunction} from 'express'
import axios, { AxiosError } from 'axios';
import { readFileSync } from 'fs';
import path from 'path';
import https from 'https';
import 'dotenv/config'
const PROXY_PORT_HTTP = process.env.PROXY_PORT_HTTPS || 80;
const PROXY_PORT_HTTPS = process.env.PROXY_PORT_HTTPS || 443;
const ENABLE_HTTP = process.env.ENABLE_HTTP || true;

interface ProxyConfig {
    regex: RegExp
    target: string
}

declare global {
    namespace Express {
        export interface Request {
            proxyContext: {
                config: ProxyConfig
                hasBody: boolean
            }
        }
    }
}

function proxy(configs: ProxyConfig[]) {
    return [
        redirectToTls,
        express.raw({type: "*/*"}), 
        attachContext(configs),
        sendRequest
    ]
}

(async () => {
    const app = express();

    app.set("json spaces", 2);

    app.use(proxy([
        {
            regex: /^\/cms..*$/,
            target: "localhost:3001"
        },
        {
            regex: /.*?/,
            target: "localhost:3000"
        }
    ]));

    if(ENABLE_HTTP) {
        app.listen(PROXY_PORT_HTTP, async () => {
            console.log(`Proxy is now listening for incoming HTTP connections on port ${PROXY_PORT_HTTP}.`);
        });
    }

    https.createServer({
        key: readFileSync(path.join(__dirname, "..", "..", "certs", "server.key")),
        cert: readFileSync(path.join(__dirname, "..", "..", "certs", "server.cert")),
        // passphrase: process.env.TLS_KEY_PASSPHRASE
    }, app).listen(PROXY_PORT_HTTPS, async () => {
        console.log(`Proxy is now listening for incoming HTTPS connections on port ${PROXY_PORT_HTTPS}.`);
    });
})();



function redirectToTls(req: Request, res: Response, next: NextFunction) {
    if(!req.secure) {
        return res.redirect(301, "https://" + req.headers.host + req.url);
    }
    next();
}


function attachContext(configs: ProxyConfig[]) {
    return (req: Request, res: Response, next: NextFunction) => {
        let path = req.path   
        if(path) {
            for(const config of configs) {
                if(config.regex.test(path)) {
                    req.proxyContext = {
                        config,
                        hasBody:  Object.keys(req.body).length != 0
                    }
                    console.log("context: ", req.proxyContext.config);
                    break;
                }
            }
        } else {
            throw new Error("NO HOST HEADER");
        }
        next();
    }
}

async function sendRequest(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        console.log(`http://${req.proxyContext.config.target}${req.originalUrl}`);
        if(req.method != "GET") {
            console.log("method: ", req.method);
            console.log("body length: ", req.body.length)
        }
        let response = await axios(`http://${req.proxyContext.config.target}${req.originalUrl}`, {
            method: req.method,
            responseType: 'arraybuffer',
            data: req.proxyContext.hasBody ? req.body : null,
            headers: req.headers,
            beforeRedirect: (options, { headers } ) => {
                console.log("before redirect triggered");
            }
        });
        let buffer = response.data as ArrayBuffer;
        if(buffer) {
            console.log(buffer.byteLength)
            for(const [k, v] of Object.entries(response.headers)) {
                res.setHeader(k, v);
            }
            res.send(buffer);
        } else {
            console.log("?")
        }
    } catch (err: any) {
        if(err.response && err.response.status == 304) {
            console.log("304 status occurred");
            res.status(304);
            res.send();
        } else {
            console.log("an error occurred");
            next(err);
        }
    }
}