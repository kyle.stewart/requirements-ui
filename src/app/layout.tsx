import type { Metadata } from 'next'
import './globals.css'
import 'flowbite'
import { ThemeModeScript } from 'flowbite-react';
 
export const metadata: Metadata = {
  title: 'Requirements UI',
  description: 'Provides a view into requirements.',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <ThemeModeScript />
      </head>
      <body className="m-10">{children}</body>
    </html>
  )
}
