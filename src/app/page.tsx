'use client';

import { Button, Modal } from 'flowbite-react';
import { useState } from 'react';

export default function Page() {
    const [openModal, setOpenModal] = useState(false);

    return (
        <>
            <h1 className="text-3xl font-bold" data-test="hero-heading">Hello, Next.js!</h1>
            <div className="p-4 my-8 text-sm text-blue-800 rounded-lg bg-blue-50 dark:bg-gray-800 dark:text-blue-400" role="alert">
                <span className="font-medium">Info alert!</span> Change a few things up and try submitting again.
            </div>
            <div className="my-8">
                <h2 className="text-2xl font-bold">Links</h2>
                <ul>
                    <li>
                        <a href="/cms/admin" target="_blank" rel="noopener noreferrer">Payload CMS Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="/cms/api/graphql-playground" target="_blank" rel="noopener noreferrer">Payload CMS GraphQL Playground</a>
                    </li>
                </ul>
            </div>

            <Button onClick={() => setOpenModal(true)}>Toggle modal</Button>
            <Modal show={openModal} onClose={() => setOpenModal(false)}>
                <Modal.Header>Terms of Service</Modal.Header>
                <Modal.Body>
                <div className="space-y-6">
                    <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                    With less than a month to go before the European Union enacts new consumer privacy laws for its citizens,
                    companies around the world are updating their terms of service agreements to comply.
                    </p>
                    <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                    The European Union’s General Data Protection Regulation (G.D.P.R.) goes into effect on May 25 and is meant
                    to ensure a common set of data rights in the European Union. It requires organizations to notify users as
                    soon as possible of high-risk data breaches that could personally affect them.
                    </p>
                </div>
                </Modal.Body>
                <Modal.Footer>
                <Button onClick={() => setOpenModal(false)}>I accept</Button>
                <Button color="gray" onClick={() => setOpenModal(false)}>
                    Decline
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
