/// <reference types="cypress" />
// ***********************************************

export {}

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Custom command to select DOM element by **data-test** attribute.
             * @example cy.getByData('greeting')
             */
            getByData(value: string): Chainable<JQuery<HTMLElement>>
        }
    }
}

Cypress.Commands.add("getByData", (selector) => {
    return cy.get(`[data-test=${selector}]`)
})