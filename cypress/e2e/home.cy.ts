describe('Home Page', () => {
  beforeEach(() => {
    cy.visit("https://localhost")
  })

  context("hero section", () => {
    it('make sure hero heading is correct', () => {
      cy.get("[data-test='hero-heading']")
        .should("exist")
        .contains("Hello, Next.js!")
    })
  })
})