# Requirements UI

This projects presents a user interface for a requirements driven design application. The project is based on a few key technologies:

* [NodeJS](https://nodejs.org/)
* [NextJS](https://nextjs.org/)
* [Payload CMS](https://payloadcms.com/)
* [ExpressJS](https://expressjs.com/)
* [MongoDB](https://www.mongodb.com/)

## Prerequisites

In order to work on this project, the primary requirement is `NodeJS`. The latest LTS version (currently `v20.10.0`) is sufficient. `OpenSSL` is required to use the commands below to create certificate authority certificates for local HTTPS development. Ensure the following commands work in the terminal.

```sh
node --version
npm --version
openssl version
git version
mongod -version
```

Once the repository is cloned locally, install the required dependencies from NPM:

```sh
npm install
```

Make a directory called `./certs` in the root of the cloned repository and execute the following command inside that folder:

```sh
openssl req -nodes -x509 -sha256 -newkey rsa:2048 -keyout server.key -out server.cert -days 365 -subj "/C=XX/ST=XX/L=None/O=None/OU=None/CN=localhost" -addext "subjectAltName = DNS:localhost,DNS:payload.localhost"
```

This should result in `server.cert` and `server.key` to be created in the folder. These are used to enable HTTPS support. Importing the certificate into the operating systems trusted store for certificate authorities will enable a fully trusted secure local development browser environment.

Next, copy the `.env.example` to `.env` and adjust any of the variable parameters as needed. The defaults are a good place to start.


## Running in Development Mode

Once the prerequisites are satisfied, starting the project is done by executing:

```sh
npm start
```

This starts three different types of web servers. A custom ExpressJS server acts as the reverse proxy and listens on localhost, ports 80 (HTTP) and 443 (HTTPS). The HTTP server is a redirect to HTTPS. NextJS is started on HTTP over port 3000 and acts as the primary user facing application server. Payload is started on HTTP over port 3001 and provides a few different endpoints for accessing content.

Here are some key endpoints:

* [https://localhost](https://localhost) - Home Page for User App
* [https://localhost/cms/admin](https://payload.localhost/admin) - Login Page for Payload Admin Dashboard
* [https://localhost/cms/api/graphql-playground](https://payload.localhost/api/graphql-playground) - GraphQL Playground

## Testing

This project uses [Cypress](https://www.cypress.io/) as its testing framework. Execute the following to launch the Cypress testing environment:

```sh
npx cypress open
```