import { buildConfig } from "payload/config";
import { viteBundler } from "@payloadcms/bundler-vite";
import { lexicalEditor } from "@payloadcms/richtext-lexical";
import { mongooseAdapter } from "@payloadcms/db-mongodb";

export default buildConfig({
  admin: {
    bundler: viteBundler(),
  },
  db: mongooseAdapter({
    url: process.env.DATABASE_URI || "mongodb://127.0.0.1/requirements-ui",
  }),
  editor: lexicalEditor({}),
  telemetry: false,
  serverURL: "https://localhost",
  routes: {
    admin: "/cms/admin",
    api: "/cms/api",
  },
  collections: [
    {
      slug: "mvs",
      labels: {
        singular: "Mission Value Statement",
        plural: "Mission Value Statements"
      },
      admin: {
        useAsTitle: "feature.title"
      },
      fields: [
        {
          name: "feature",
          type: "group",
          interfaceName: "feature",
          fields: [
            {
              name: "classification",
              type: "select",
              options: [
                "Unclassified"
              ],
              required: true
            },
            {
              name: "title",
              type: "text",
              required: true,
            },
            {
              name: "narrative",
              type: "text",
              required: true,
              admin: {
                placeholder: "As a ______, I want to _____ so I can _______"
              }
            }
          ]
        },
        {
          name: "scenarios",
          type: "array",
          interfaceName: "scenarios",
          fields: [            
            {
              name: "classification",
              type: "select",
              options: [
                "Unclassified"
              ],
              required: true
            },
            {
              name: "title",
              type: "text",
              required: true,
            },
            {
              name: "criteria",
              label: "Acceptance Criteria",
              type: "richText",
              required: true,
            },
          ]
        },
      ],
    },
  ],
  globals: [
  ],
});
